# Hero Test

Kleines Test Projekt mit einem Spring Backend und einem Angular2 Frontend

## Installation




## Entwicklung

* [Gradle](https://gradle.org) (Buildtool)
* [NodeJS](https://nodejs.org) (Laufzeitumgebung für JavaScript + NPM Paketmanager)
* [SourceTree](https://www.sourcetreeapp.com/) (GIT Client)
* **AngularCLI**: Wird über NPM installiert: `npm i -g angular-cli`

Um JSONs im Chrome ordentlich anzuzeigen, kann das Plugin [JSONView](https://chrome.google.com/webstore/detail/jsonview/chklaanhfefbnpoihckbnefhakgolnmc) installiert werden.
Um POST-Requests zu testen kann die Chrome App [Postman](https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop) verwendet werden. 

### Frontend

Mit `gradle npmInstall` werden alle benötigten Abhängigkeiten heruntergeladen.

Mit `gradle npm_start` um den Frontend-Server zu starten. Dieser ist dann unter <http://localhost:4200> zu erreichen.

### Backend

Entwicklung des Backends

#### Eclipse

Wenn zum Entwickeln des Backends [Eclipse](https://eclipse.org) verwendet werden soll, kann mit Gradle das Projekt erstellen werden:

`gradle eclipse`

Danach kann in Eclipse mit 'File -> Import... -> Existing Projects into Workspace' das Projekt importiert werden.

#### Grundlagen

Artikel zum Erstellen eines RESTful Webservices: <https://spring.io/guides/gs/rest-service/>

#### Server starten

Der Backend-Server kann mit `gradle bootRun` gestartet werden und ist dann unter <http://localhost:8080> erreichbar. Unter <http://localhost:8080/api/heros> sollten alle Heros aufgelistet werden.

Um den Backend-Server bei Änderungen am Sourcecode automatisch neu zu starten, muss noch `gradle classes -t` gestartet werden.

#### Datenbank

Wenn der Server gestartet ist, kann über <http://localhost:8080/h2-console> auf die Datenbank zugegriffen werden. Als
'JDBC URL' muss `jdbc:h2:mem:testdb` eingegeben werden.

## Continues Integration

Ein Jenkins-Server zum automatischen Bauen der Applikation ist unter <http://hero-test.mircloud.host/jenkins/> installiert.

Der Login ist `admin:jenkins1`