package server.api.hero;

public class Hero {

	public int id = 0;
	public String name;
	public String description;
	
	protected Hero() {
	}
	
	public Hero(int id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}

	public Hero(Hero hero) {
		this.id= hero.id;
		this.name = hero.name;
		this.description = hero.description;
	}

}
