package server.api.hero;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HeroController {

	@Autowired
	HeroRepository registry;

	@RequestMapping("/heros")
	public List<Hero> getHeros() {
		return this.registry.list();
	}
	
	@RequestMapping(path = "/hero", method = RequestMethod.PUT)
	public Hero addHero(@RequestBody Hero hero) {
		return this.registry.add(hero);
	}
	
	@RequestMapping("/hero/{id}")
	public Hero getHero(@PathVariable int id) {
		return this.registry.get(id);
	}
	
	@RequestMapping(path = "/hero/{id}", method = RequestMethod.DELETE)
	public void  delHero(@PathVariable int id) {
		this.registry.del(id);
	}
	
}
