package server.api.hero;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import util.DBUtil;

@Service
public class HeroRepository {

	@Autowired
	DataSource dataSource;

	public HeroRepository() {
	}

	public List<Hero> list() {
		try (Connection conn = this.dataSource.getConnection()) {
			return list(conn);
		} catch (SQLException e) {
			throw new RuntimeException("Error loading heros from database: " + e.getMessage(), e);
		}
	}

	public List<Hero> list(Connection conn) throws SQLException {
		List<Hero> heros = new ArrayList<Hero>();

		try (ResultSet rslt = DBUtil.executeQuery("select * from hero", conn)) {
			while (rslt.next()) {
				int id = rslt.getInt("id");
				String name = rslt.getString("name");
				String description = rslt.getString("description");
				heros.add(new Hero(id, name, description));
			}
		}
		return heros;
	}

	public Hero add(Hero hero) {
		try (Connection conn = this.dataSource.getConnection()) {
			return add(hero, conn);
		} catch (SQLException e) {
			throw new RuntimeException("Error adding hero to database: " + e.getMessage(), e);
		}

	}

	public Hero add(Hero hero, Connection conn) throws SQLException {

		try (PreparedStatement stmt = conn.prepareStatement("insert into hero (name,description) values (?,?)",
				Statement.RETURN_GENERATED_KEYS)) {
			stmt.setString(1, hero.name);
			stmt.setString(2, hero.description);
			stmt.executeUpdate();
			try (ResultSet rslt = stmt.getGeneratedKeys()) {
				rslt.next();
				int newId = rslt.getInt(1);
				return get(newId);
			}
		}
	}

	public Hero get(int id) {
		try (Connection conn = this.dataSource.getConnection()) {
			return get(id, conn);
		} catch (SQLException e) {
			throw new RuntimeException("Error get hero with id " + id + " from database: " + e.getMessage(), e);
		}
	}

	public Hero get(int id, Connection conn) throws SQLException {

		try (ResultSet rslt = DBUtil.executeQuery("select * from hero where id=" + id, conn)) {
			if(!rslt.next())
			{
				throw new RuntimeException("No hero with " + id );
			}
			String name = rslt.getString("name");
			String description = rslt.getString("description");
			Hero hero = new Hero(id, name, description);
			return hero;
			

		}

	}

	public void del(int id) {
		try (Connection conn = this.dataSource.getConnection()) {
			del(id, conn);
		} catch (SQLException e) {
			throw new RuntimeException("Error deleting hero with " + id + " from database: " + e.getMessage(), e);
		}

	}

	public void del(int id, Connection conn) throws SQLException {
		try (PreparedStatement stmt = conn.prepareStatement("delete from hero where id=?")) {
			stmt.setInt(1, id);
			stmt.executeUpdate();
		}
	}
}
