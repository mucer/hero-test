package server.app;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {

	private Path htmlDir;

	public IndexController() throws URISyntaxException {
		this.htmlDir = Paths.get(IndexController.class.getResource("/html").toURI());
	}

	@RequestMapping("/**")
	HttpEntity<byte[]> home(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String fileName = request.getServletPath().substring(1);

		Path path = this.htmlDir.resolve(fileName);
		File file = path.toFile();
		if (!file.exists()) {
			path = this.htmlDir.resolve("index.html");
		} else if (file.isDirectory()) {
			path = path.resolve("index.html");
		}

		byte[] bytes = Files.readAllBytes(path);
		if ("index.html".equals(path.getFileName().toString())) {
			String context = request.getContextPath();
			System.out.println("context=" + context + ", bla=" + request);
			bytes = new String(bytes).replaceAll("<base href=\"[^\"]*\">", "<base href=\"" + context + "/\">")
					.getBytes();
		}

		return new HttpEntity<byte[]>(bytes);
	}
}
