package util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBUtil {
	public static ResultSet executeQuery(String sql, Connection conn) throws SQLException {
		Statement stmt = conn.createStatement();
		return stmt.executeQuery(sql);
	}

	public static void closeResultSet(ResultSet rslt) throws SQLException {
		if (rslt != null) {
			Statement stmt = rslt.getStatement();
			if (!rslt.isClosed()) {
				rslt.close();
			}
			if (stmt != null && stmt.isClosed()) {
				stmt.close();
			}
		}
	}
}
