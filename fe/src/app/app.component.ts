import { Component } from '@angular/core';

import { HeroService } from './hero.service';
import { Hero } from './hero';

@Component({
  selector: 'app-root',
  template: `
    <h1>
      {{title}}
    </h1>

    <ul>
      <li *ngFor="let hero of service.heros; trackBy: getHeroId">
        {{ hero.name }} 
        <span class="description">{{ hero.description }}</span>
        <button (click)="delete(hero)">x</button>
      </li>
    </ul>
    <input placeholder="Name" type="text" [(ngModel)]="newHero.name"/><button (click)="add()">+</button>
  `,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';

  private newHero:Hero = {};

  constructor(private service: HeroService) {
  }

  ngOnInit() {
    this.service.loadHeros();
  }

  delete(hero: Hero) {
    this.service.deleteHero(hero);
  }

  add(hero: Hero) {
    if (this.newHero.name) {
      this.service.addHero(this.newHero);
      this.newHero = {};
    }
  }

  getHeroId(hero:Hero) {
    return hero.id;
  }
}
