import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Hero } from './hero';
import 'rxjs/add/operator/map';

@Injectable()
export class HeroService {
	heros: Hero[];

	constructor(private http: Http) {
	}

	loadHeros() {
		 this.http.get('api/heros')
		 	.map(r => r.json())
			.subscribe(h => this.heros = h);

			// .subscribe(function(h:Hero[]) {
			//   this.heros = h;
			// }
	}

	deleteHero(hero: Hero) {
		this.http.delete('/api/hero/' + hero.id).subscribe(() => {
			// this.heros = this.heros.filter(h => h.id !== hero.id);
			this.loadHeros();
		});
	}

	addHero(hero:Hero) {
		this.http.put('/api/hero', hero).subscribe(() => {
			this.loadHeros();
		});
	}
}